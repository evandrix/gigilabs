﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionBodiedPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person("Chuck", "Norris");
            Console.WriteLine(person.FullName);

            Console.ReadLine();
        }
    }
}
