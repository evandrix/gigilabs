﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionBodiedSingleton
{
    class Program
    {
        static void Main(string[] args)
        {
            var dbManager = DbManager.Instance;
            dbManager.Open();

            Console.WriteLine("Doing something...");

            dbManager.Close();

            Console.ReadLine();
        }
    }
}
