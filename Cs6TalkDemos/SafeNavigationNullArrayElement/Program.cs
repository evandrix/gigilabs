﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeNavigationNullArrayElement
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new string[] { "Chuck", null, "Bob" };
            var second = names[1]?.Length;
        }
    }
}
