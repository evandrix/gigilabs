﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexInitializers
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<char, string> morse = new Dictionary<char, string>()
            {
                ['A'] = ".-",
                ['B'] = "-...",
                ['C'] = "-.-.",
                ['D'] = "-..",
                ['E'] = ".",
                ['F'] = "..-.",
                ['G'] = "--.",
                ['H'] = "....",
                // ...
            };

            Console.WriteLine(morse['H']);
            Console.ReadLine();
        }
    }
}
