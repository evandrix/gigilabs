﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeNavigationProperties
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new MultiUserApplication();
            string sessionId = app.SessionId;
            Console.WriteLine(app.SessionId);

            // safe navigation and null-coalesce

            int length = sessionId?.Length ?? 0;

            // wait for user input before exiting

            Console.ReadLine();
        }
    }
}
