﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameOfArgumentNullException
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var person = new Person("Joe", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine(); // wait for user input before exiting
        }
    }
}
