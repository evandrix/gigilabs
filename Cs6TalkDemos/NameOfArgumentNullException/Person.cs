﻿using System;

namespace NameOfArgumentNullException
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Person(string firstName, string surname)
        {
            if (firstName == null)
                throw new ArgumentNullException(nameof(firstName));
            if (surname == null)
                throw new ArgumentNullException(nameof(surname));

            this.FirstName = firstName;
            this.LastName = surname;
        }
    }
}
