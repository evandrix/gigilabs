﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringInterpolation
{
    class Program
    {
        static void Main(string[] args)
        {
            var dateOfBirth = new DateTime(2014, 10, 12);
            var customer = new Customer(1354, "Tony", "Smith", dateOfBirth);

            var fullName = "\{customer.FirstName} \{customer.LastName}";

            var formattedDateOfBirth = "\{dateOfBirth : "yyyy-MM-dd"}";

            Console.WriteLine(fullName);
            Console.WriteLine(formattedDateOfBirth);

            Console.ReadLine();
        }
    }
}
