﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AppSettingsFramework.Library;

namespace AppSettingsFramework.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var reader = new AppSettingReader();
            var provider = new ConfigKeyProvider(reader);

            int timeout = provider.Get<int>("timeoutInMilliseconds", 3000);
            bool enabled = provider.Get<bool>("enabled", false);
        }
    }
}
